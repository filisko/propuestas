---
layout: 2021/post
section: proposals
category: devrooms
community: COMUNIDAD
title: NOMBRE-SALA
---

## Descripción de la sala

[DESCRIPCIÓN DE LA SALA: Explicación de la temática de la sala y la motivación para organizarla. Ten en cuenta que la idea de la sala es realizar una actividad semiautónoma dentro de esLibre, que tendrá normalmente su propia petición de contribuciones, sus propios organizadores, su propio horario (coordinado con el de esLibre), etc.]

## Comunidad que la propone

**[Nombre de la comunidad que la propone]**

[DESCRIPCIÓN DE LA COMUNIDAD: Explicación de la temática de la sala y la motivación para organizarla. Ten en cuenta que la idea de la sala es realizar una actividad semiautónoma dentro de esLibre, que tendrá normalmente su propia petición de contribuciones, sus propios organizadores, su propio horario (coordinado con el de esLibre), etc.]

-   Web de la comunidad: [URL]
-   Mastodon (u otras redes sociales libres): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio o GitHub (u otros sitios de código colaborativo): [URL]

### Contacto(s)

-   Nombre de contacto: [NOMBRE]
-   Email de contacto: [EMAIL]

## Público objetivo

[PÚBLICO OBJETIVO: ¿A quién va dirigida?]

## Formato

[FORMATO: Formato de la sala y cómo se va a hacer la solicitud de propuestas. El formato se deja totalmente abierto a decisión de la organización de la sala, pudiendo incluir: talleres, charlas cortas/largas, mesas redondas, hackathones... ]

## Preferencia horaria

-   Duración: [Indicar la duración de la propuesta. Por ejemplo, desde dos horas hasta un día entero.]
-   Día: [Indicar si se prefiere que sea el primer día o el segundo.]

## Comentarios

[COMENTARIOS: Cualquier otro comentario relevante para la organización.]

## Preferencias de privacidad

(Si queréis que vuestra información de contacto sea anónima, mandadnos las propuesta mediante el formulario de la web: <https://eslib.re/2021/propuestas/salas/>)

-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
