---
layout: 2020/post
section: proposals
category: talks
title: ASSAP&#58 Anti Shoulder Surfing Attack Platform
---

La ciberseguridad es un ámbito en que las herramientas suelen estar enfocadas a ser manejadas por gente con conocimientos técnico. Esto deja en muchos casos a usuarios fuera del punto de mira de las medidas de seguridad. Con esto en mente, el equipo de ASSAP, decidimos hace ya casi año y medio el crear una solución que pudiese formar parte de las herramientas que el usuario tiene para protegerse con dos cosas en mente: la usabilidad y que su uso pueda ser gratuito, sin gastos añadidos y universal a cualquier dispositivo. De ahí surge ASSAP, una herramienta destinada a prevenir el shoulder surfing, un ataque que supone que alguien está mirándote por encima del hombro al teclear. Esto parece sencillo, pero detectar una cara, ser capaz de identificar a que persona pertenece y las medidas que tomar en caso de que esto suceda no son fáciles de plantear o realizar. En esta charla te enseñaremos como encara ASSAP estos retos usando DeepLearning, Electron y Vue.js

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

[ASSAP](https://github.com/assap-org/assap) desde un inicio nace como un proyecto distinto a lo usual, un caso de uso que está a caballo entre el mundo digital y el plano físico. Tecnologías en el estado del arte que suponen un esfuerzo de investigación y adaptación quizá demasiado arriesgado. Un público objetivo difícil de alcanzar debido a que el ataque a la privacidad que se quiere remediar está ampliamente normalizado y no existe una preocupación real por ponerle remedio.

Esto son las premisas de ASSAP... y probablemente de gran parte de los proyectos que buscan solucionar problemas relacionados con la ciberseguridad y la privacidad enfocándose en que el usuario sea capaz de entenderlos, usarlos y estar un poco más seguro en el día a día.

De un contexto tan singular, ASSAP ha supuesto una experiencia de aprendizaje y perseverancia para todo el equipo de desarrolladores. En esta charla hablaremos sobre usabilidad, sobre lo que aprendimos al simplificar al máximo la idea y la interfaz, sobre cómo lidiar con tecnologías en el estado de arte, sobre lo que supone iniciar un proyecto de código libre y responsabilizarse de él y por supuesto sobre lo que hay en el código: las tecnologías que hemos usado, las librerías, las partes a mejorar, en que seguimos trabajando y los puntos que queremos evolucionar.

Esperamos que esta charla pueda ser una forma de atraer gente a colaborar con nuestro proyecto, usar nuestra herramienta y darnos sus puntos de vista sobre qué podemos mejorar, qué creen que podríamos haber hecho mejor y qué les parece más interesante de nuestro proyecto.

## Público objetivo

Esta charla está enfocada a cualquier persona que esté interesada por el software libre o la privacidad. No es necesario tener conocimientos técnicos pese a que habrá partes en las que se tratarán fragmentos de código en el lenguaje más cercano al humano posible.

## Ponente(s)

Soy Jorge Cuadrado Saez, [@coke727](https://twitter.com/Coke727), investigador en ciberseguridad en el laboratorio de innovación de BBVA Next Technologies. Estudié Ingeniería Informática en la Universidad de Valladolid (UVA) y el Máster en Ciberseguridad de la Universidad Carlos III de Madrid (UC3M). He participado como ponente en múltiples congresos como: Cybercamp, RootedCon, Hack In The Box, X1RedMásSegura o JASyP.

Mis focos de interés, sobre los que siempre tengo algún proyecto en marcha, son: privacidad, impresión 3D y electrónica.

### Contacto(s)

-   Jorge Cuadrado Saez: jorge.cuadrado.saez at gmail dot com | [@coke727](https://gitlab.com/coke727)

## Comentarios

Necesitaré proyector y enchufe.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
