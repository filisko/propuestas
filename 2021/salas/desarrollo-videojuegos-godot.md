---
layout: 2021/post
section: proposals
category: devrooms
title: Desarrollo de videojuegos con Godot
---

## Descripción de la sala

Una sala informal para aprender más del motor de desarrollo de videojuegos libre Godot y hacer piña con gente interesada en el desarrollo de videojuegos con software libre.

## Comunidad que la propone

Sala organizada por Andrés Ortiz. Esta sala está organizada de forma informal por entusiastas de Godot en colaboración con LibreLabGRX y no es parte de ninguna comunidad oficial de Godot.

-   Web de la comunidad: <https://godotengine.org/>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab: <https://gitlab.com/angrykoala>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/godotengine/>

### Contacto(s)

-   Nombre de contacto: Andrés Ortiz
-   Email de contacto: <angrykoala@outlook.es>

## Público objetivo

Interesados en el desarrollo de videojuegos (particularmente haciendo uso de software libre) de cualquier nivel.

## Formato

**Puedes encontrar el programa de esta sala en: <https://propuestas-godot.eslib.re/2021/>**

El formato consistirá en charlas de iniciación a Godot, charlas más especificas de Godot y en general de desarrollo de videojuegos usando software libre. También se intentará organizar un hackathon/jam con showcase de proyectos.

## Comentarios
