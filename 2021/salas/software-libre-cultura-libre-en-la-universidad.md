---
layout: 2021/post
section: proposals
category: devrooms
title: Software Libre y Cultura Libre en la universidad
---

## Descripción de la sala

El objetivo de esta propuesta es discutir el estado actual del software libre en las universidades españolas y como se podría reconducir el mismo para volver a revitalizarlo. Después de una época que comenzó hace unos 15 años con muchas universidades animándose a apoyar "eso del software libre", pocas siguen operativas hoy en día: algunas porque nunca llegó a existir un interés real por parte de las propias universidades más allá de aparecer en clasificaciones como el RuSl ([Ranking de universidades en Software Libre](https://www.portalprogramas.com/software-libre/ranking-universidades/), otras porque perdieron el tibio apoyo de un determinado equipo de gobierno con la llegada de nuevas elecciones rectorales...

El escenario puede parecer poco esperanzador, sin embargo, nunca han dejado de existir iniciativas que se resisten a rendirse ante la defensa de la importancia y la necesidad de las tecnologías y la cultura libres en la educación y la ciencia.

Precisamente por eso queremos crear un espacio donde podamos comentar los problemas actuales, así como trazar posibles líneas de actuación que incluyeran a todos los colectivos que representan la comunidad universitarias: estudiantes, personal y profesores.

## Comunidad que la propone

#### Oficinas y asociaciones universitarias de software libre

Toda persona que tenga ideas, iniciativas, proyectos o experiencias en este ámbito es bienvenida, pero para ir poniendo ideas en común, nos estamos agrupando en grupo de personas que nos podrán comentar sus experiencias personales desde:

- Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos
- Oficina de Software Libre de la Universidad de Granada
- Oficina de Software Libre de la Universidad de La Laguna
- Oficina de Software Libre de la Universidad de Zaragoza
- Aula de Software Libre de la Universidad de Córdoba
- LibreLabUCM (Universidad Complutense de Madrid)
- SUGUS GNU/Linux (Universidad de Sevilla)
- Concurso Universitario de Software Libre


- Universidad de Cádiz
- Universidad de Castilla La Mancha
- Universidad de Córdoba
- Universidad de Granada
- Universidad de La Laguna
- Universidad de Las Palmas de Gran Canaria
- Universidad de Oviedo
- Universidad de Sevilla
- Universidad de Zaragoza
- Universidade da Coruña
- Universidade de Santiago de Compostela
- Universidade de Vigo
- Universidad Rey Juan Carlos
- Universitat Autònoma de Barcelona


Grupos que apoyan activamente esta iniciativa:
- LibreLabGRX
- Interferencias

-   Web de la comunidad: <https://eslib.re/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@eslibre/>
-   Twitter: <https://twitter.com/esLibre_>
-   GitLab: <https://gitlab.com/eslibre>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: Germán Martínez
-   Email de contacto: <germaaan@interferencias.tech>

## Público objetivo

Cualquier persona que tenga la inquietud porque se fomente el software libre en la educación en general.

## Formato

La programación de la sala se repartirá entre 3 bloques durante ambos días del congreso:

#### VIERNES 25
#### 09:50-11:00 SESIÓN PLENARIA: "SOFTWARE LIBRE Y CULTURA LIBRE EN LA UNIVERSIDAD"

* Sesión de apertura del congreso donde se realizarán varias propuestas y experiencias personas del ámbito de cultura libre en las Universidades. Preámbulo para el desarrollo de la sala durante la jornada del sábado.
* Participaciones:
    * 09:50-10.10: "Repensando las oficinas de software libre en las Universidades" por Jesús Gonzalez-Barahona
    * 10:10-10:40: "Cultura libre, universidad y sociedad. TXS.es, una fundación necesaria" por Luis Fajardo
    * 10:40-11:00: "Experiencias desde el mundo del software libre en la Universidad" (ideas en 5 minutos)
        * Ángel Bailo
        * Pablo García
        * Paula de la Hoz
        * JJ Merelo

#### SÁBADO 26
#### 11:00-12:15 RELATO COLABORATIVO: "¿CÓMO PROMOVER EL SOFTWARE LIBRE EN LA UNIVERSIDAD?"

* **Debate**: partiendo de la pregunta “¿Cómo promover el software libre en la universidad?” se planterá un debate a micro abierto que esperamos permita, al mismo tiempo, poner en contacto a toda la comunidad interesada en la cultura libre en el ámbito universitario, hacer un análisis conjunto de la situación actual del software libre en las universidades y favorecer una primera lluvia de ideas y propuestas o acciones para la promoción del software, el conocimiento y la cultura libre en las Universidades.

* La participación es libre. Simplemente bastará con pedir el turno de palabra y presentarse brevemente, dar un poco de contexto personal sobre el interés en el tema e indicar aquellos temas no recogidos en el programa y la estructura preparada y presentada previamente para la sesión que piensen que es importante tratar.

* En todo momento habrá una persona que se encargará de presentar y conducir la sesión de forma ordenada, intentando además hacer un estricto control del tiempo de las intervenciones para facilitar todas las participaciones.

* También habrá alguien que se encargue de ir tomando nota de las ideas que se lancen. La intención es que estas notas sean muy sintéticas para que sirvan como punto de partida de la siguiente sesión de debate.

#### 12:30-13:45 REFLEXIÓN / DISCUSIÓN ABIERTA: "¿CÓMO PROMOVER EL SOFTWARE LIBRE EN LA UNIVERSIDAD?"

* **Debate**: tras un breve resumen de las notas de la sesión previa, esta nueva sesión también tomará la forma de debate abierto, pero esta vez propiciando que se pueda hablar sosegadamente sobre las posibles acciones/propuestas/ideas que surjan o que hayan surgido durante las jornadas para "promover el Sofware Libre en la Universidad".

* En este espacio se intentará que la moderación sea mínima con la intención de generar una conversación más fluida y natural entre todas las personas presentes que quieran participar activamente.

## Comentarios
