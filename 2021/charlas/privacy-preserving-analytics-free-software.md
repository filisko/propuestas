---
layout: 2021/post
section: proposals
category: talks
author: Nathan Freitas
title: Privacy-Preserving Analytics for Free Software
---

Can free software be measured without introducing harm? Recently, Audacity, the most popular free software audio editing software, caused a ruckus by adding Google and Yandex tracking code to its software. While their motivation was to be able to understand the usage of their app better, their approach to the community engagement and choice of toolkits was naive. What if there was a more free and private solution, along with a smarter way of engaging users through consent-drive collaboration?

## Proposal format

- Plenary talk

## Description

Clean Insights gives developers a way to plug into a secure, private measurement platform. It is focused on assisting in answering key questions about app usage patterns, and not on enabling invasive surveillance of all user habits. Our approach provides programmatic levers to pull to cater to specific use cases and privacy needs.

It also provides methods for user interactions that are ultimately empowering instead of alienating. It is available as a lightweight, minimal impact, freely licensed toolkit to include in your mobile app, desktop app, website or back-end service. This code can be integrated into your application or service to measure specific events and interactions that you want to gain more insight on.

-   Web del proyecto: <https://cleaninsights.org> / <https://guardianproject.info>

## Target audiences



## Speaker/s

Nathan is the founder and director of Guardian Project, an award-winning free software mobile security collaborative with millions of users and beneficiaries worldwide. Their most well known app is Orbot, which brings the Tor anonymity and circumvention network to Android devices, and has been installed more than 20 million times.

In late 2017, he co-designed an app called Haven with Edward Snowden; Haven works as a personal security system that puts the power of surveillance back into the hands of the most vulnerable and under threat.

His work on off-grid, decentralized, secure mobile communication networks, dubbed Wind, was originally imagined and workshopped while Nathan was a fellow at the Berkman-Klein Center in 2015. In 2018, Wind was selected as a finalist in the Mozilla-National Science Foundation "Wireless Innovation for a Networked Society (WINS)" Challenge.

### Contact/s

-   Name: Nathan Freitas
-   Email: <nathan@freitas.net>
-   Personal website: <https://nathan.freitas.net/>
-   Mastodon (or other free social networks):
-   Twitter: <https://twitter.com/n8fr8>
-   Gitlab:
-   Portfolio or GitHub (or other collaborative code sites): <https://github.com/n8fr8>

## Comments
