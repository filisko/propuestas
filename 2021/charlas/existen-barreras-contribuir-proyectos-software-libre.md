---
layout: 2021/post
section: proposals
category: talks
author: Daniel Izquierdo
title: ¿Existen barreras para contribuir a proyectos de software libre?
---

Durante los últimos meses he tenido la oportunidad de trabajar junto a la ASF y a la Universidad Estatal de Oregón en entender en profundidad las barreras a las que se enfrentan los contribuidores cuando participan en proyectos de software libre.

Esta charla desgranará estas barreras que se dividen en tres grandes grupos: de tipo técnico, relacionado con procesos y cuestiones más sociales. Estos a su vez se dividen en otros subgrupos hasta llegar a más de 80.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

El grupo de trabajo de diversidad e inclusión de la Fundación Apache tiene como objetivo el entender en profundidad y desde un punto de vista social a los participantes de sus comunidades y dedicar esfuerzos a entender y promocionar la diversidad e inclusión en sus proyectos.

Su misión principal es la de construir entornos donde todas las personas se sientan seguras y puedan participar en igualdad de oportunidades.

Para ello, han realizado un estudio sobre sus comunidades que ha consistido por un lado en una encuesta con alrededor de 700 respuestas y entrevistas para conocer en profundidad las necesidades de ciertos grupos de personas que de forma recurrente están subrepresentadas en la industria.

Esta charla detallará los diferentes resultados y conclusiones a las que se ha llegado y que esperamos que sean útiles para otras comunidades de software libre con el objetivo de construir comunidades más inclusivas.

-   Web del proyecto: <https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=130026504>

## Público objetivo

Cualquier persona interesada en el proceso de contribución en las comunidades de software libre y en entender las dificultades a las que se pueden enfrentar diferentes grupos sociales.

## Ponente(s)

Daniel Izquierdo Cortázar

### Contacto(s)

-   Nombre: Daniel Izquierdo
-   Email: <dizquierdo@bitergia.com>
-   Web personal: <https://bitergia.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/dizquierdo>
-   GitLab: <https://gitlab.com/dicortazar>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/dicortazar>

## Comentarios
