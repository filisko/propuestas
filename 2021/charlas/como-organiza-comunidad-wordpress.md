---
layout: 2021/post
section: proposals
category: talks
author: Ángel Moreno / Francisco Torres
title: Cómo se organiza la Comunidad WordPress
---

WordPress es un software libre, como sabéis, pero también es una gran comunidad de personas con una gran organización desarrollada a lo largo de muchos años. Hablaremos de meetups locales, WordCamps pequeñas y grandes, y las herramientas colaborativas que la comunidad utiliza para dinamizar a su gente, desarrollar software, traducciones, y todo lo relacionado.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

¿Recordáis cuando las kdds, jornadas y congresos eran en persona? Qué nostalgia... Quizá por eso nos ponemos en modo abuelo Cebolleta (la propia referencia ya es de alerta viejuner) para hacer un recorrido en primera persona por la Comunidad WordPress.

En Granada tenemos a Fran Torres, una persona (aunque cuando se queda muy quieto parece cosa) reconocida en la comunidad internacional WordPressera además de ser ingeniero informático y licenciado en comunicación audiovisual, al que "liaron" la gente de WordPress Sevilla (muy potentes internacionalmente) para asistir a la primera WordCamp Europe en 2013 (Leiden, Holanda). Al año siguiente, Fran "lió" a Ángel Moreno (un friki granaíno de amplio espectro y baja intensidad) para asistir a la europea de Sofia, Bulgaria. Y ya todo se fue liando poco a poco.

Entonces no tanta gente usaba WordPress, y lógicamente menos participaban activamente en la comunidad, por lo que todo era como más recogido. Casi que conocías a todo el mundo, de WordCamp en WordCamp, y se iba tejiendo una red de amistades, colaboraciones, intereses comunes por un software y todas sus cuestiones satélites que nos daban de comer (y beber), además de disfrutar ayudando, compartiendo conocimiento libre.

Apareció la Comunidad WordPress Granada, a la que se sumaron personas con mucho empuje, gracias a las que durante varios años se ha venido realizando una meetup mensual sobre muy diferentes temas relacionados: más técnicos , talleres desarrollando plugins para liberarlos, o de marketing, diseño, sesiones para resolver dudas a los recién llegados...

WordPress son las personas que lo hacen, es mucho más que un CMS, y queremos compartir esta experiencia con todas y todos, poner algunas caras también a los creadores de tal o cual plugin que quizá usas, o quién desarrolló personalmente tal cosa que tanto odias XDDD ¡Nada es perfecto! Lo bueno, es que todo está en evolucío constante. Cualquiera puede colaborar. O incluso forkearlo.

-   Web del proyecto: <https://wpgranada.es/>

## Público objetivo

Cualquier persona con interés en conocer cómo funciona una comunidad libre internacional, y el desarrollo de WordPress en particular.

## Ponente(s)

- Francisco Torres: [@frantorres](https://twitter.com/frantorres)
- Ángel Moreno: [@arkangel](https://twitter.com/arkangel)

### Contacto(s)

-   Nombre: Ángel Moreno
-   Email: <amoreno@si2.info>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/arkangel>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
