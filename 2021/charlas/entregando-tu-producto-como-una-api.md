---
layout: 2021/post
section: proposals
category: talks
author: Tomas Garzón
title: Entregando tu producto como una API
---

Hoy en día es muy común entregar tu producto o plataforma como un servicio, donde encontramos distintos servicios trabajando juntos e interaccionando para llegar a un objetivo común. Por tanto, tener una API muy poderosa puede ser la clave para una buena integración dentro del ecosistema.

El diseño de la API dependerá de la interacción que esperes tener. Se pueden aplicar patrones de diseño distintos para una API consumida por humanos (app movil) o aplicar otras reglas si se trata de servicios.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Se presentará en detalle distintos problemas y soluciones que se han ido aplicando para poder construir una API REST sobre la que se conecten otros servicios o una aplicación movil cliente.

Hablaré de temas de actualidad relacionadas con el desarrollo de API, usando como tecnología Django y el paquete Django REST Framework. Se tratarán temas de autenticación y autorización, sobrecarga de API, limite de llamadas, routers heredados, testing, etc.

-   Web del proyecto:

## Público objetivo

Desarrolladores con conocimientos en APIs REST.

## Ponente(s)

- Desarrollador software backend desde 2006, con experiencia en muchos proyectos de startups, siempre usando python y Django como stack tecnológico. Actualmente trabajo como desarrollador senior backend en Nucoro.

- Fui ponente en esLibre hace unos años y ponente en una charla de PyConES.

### Contacto(s)

-   Nombre: Tomas Garzón
-   Email: <tomasgarzonhervas@gmail.com>
-   Web personal: <https://www.linkedin.com/in/tomasgarzonhervas>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
