---
layout: 2021/post
section: proposals
category: talks
author: Lorenzo Carbonell
title: Hacia el minimalismo, la personalización y los entornos altamente productivos&#58 gestores de ventanas tipo mosaico
---

Los entornos de escritorio cada día se van haciendo mas complejos y pesados, y no por ello mas productivos.

¿Que son los gestores de ventanas tipo mosaico o Tiling Window Manager?¿Qué diferencias tienen con los entornos de escritorio tradicionales?

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

El objetivo de esta charla es dar una visión general sobre los gestores de ventana tipo mosaico y las diferencias con los entornos de escritorio tradicionales.

La charla se centrará en un gestor de ventana particular como es BSPWM y algunas herramientas complementarias como Polybar y Rofi, para construir un entorno altamente productivo.

-   Web del proyecto: <https://www.atareao.es/tutorial/polybar>

## Público objetivo

Cualquier usuario interesado en mejorar su productividad ante el ordenador.

## Ponente(s)

Lorenzo Carbonell:

- Sobre mi: <https://www.atareao.es/quien-soy/>

### Contacto(s)

-   Nombre: Lorenzo Carbonell
-   Email: <atareao@atareao.es>
-   Web personal: <https://www.atareao.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@atareao>
-   Twitter: <https://twitter.com/atareao>
-   GitLab: <https://gitlab.com/atareao>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/atareao>

## Comentarios
