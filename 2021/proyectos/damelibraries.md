---
layout: 2021/post
section: proposals
category: projects
title: DameLibraries
---

DameLibraries is a collection of snippets oriented to learn python libraries from tests.

-   Web del proyecto: <http://damelibraries.davidam.com>

### Contacto(s)

-   Nombre: David Arroyo
-   Email:
-   Web personal: <http://davidam.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab: <https://gitlab.com/davidam1>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/davidam>

## Comentarios
