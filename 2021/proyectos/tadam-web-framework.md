---
layout: 2021/post
section: proposals
category: projects
title: Tadam Web Framework
---

Tadam es un Framework minimalista para la creación de sitios web dinámicos orientados a la programación funcional. Todo el potencial de Clojure simplificado para un rápido desarrollo. Podría ser de alguna manera equivalente a Flask en el ecosistema de Python, pero algo más moderno y modular.

-   Web del proyecto: <https://www.tadam-framework.dev/>

### Contacto(s)

-   Nombre: Andros Fenollosa Hurtado
-   Email: <andros@fenollosa.email>
-   Web personal: <https://programadorwebvalencia.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.technology/@androsfenollosa>
-   Twitter: <https://twitter.com/androsfenollosa>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/tanrax/>

## Comentarios
